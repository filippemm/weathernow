# WeatherNow

This is a simple project that show 3 cities weather using [API openweathermap.org](openweathermap.org)

## System Environment

- [Angular CLI](https://github.com/angular/angular-cli) version 12.0.4;
- [Git](https://git-scm.com) 2.30.1 (or higher);
- [Node](https://nodejs.org/) 14.15.4 (or higher).

Take care to verify that the Angular version is compatible with the Node version.
## Development server

### Run development server
1) Install dependencies with ```npm install```.

2) Enter your [FREE API KEY](https://openweathermap.org/price)  in the ```src/environments/environment.ts``` file on ```openWeatherMapKey``` variable.

3) Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Run tests
Tests were done with [Karma](https://angular.io/guide/testing-components-basics) for examples and more information in [Angular click here](https://angular.io/guide/testing-components-basics).

Run ```npm test``` for a component tests.

### Change Chosen Cities
If you want you can change as chosen cities, just go to ```src/app/api/weather/components/weather/weather-list/weather-list.component.ts``` file and overwrite ```locations``` variable.
## Future plans

- Refactor .sass to join styles into better-grouped classes
- Bug fix: safari favicon
- Improve mobile experience, because when loading the page on small phones it's hiding part of the first card
