export interface SimpleWeather {
  temp: number,
  pressure: number,
  humidity: number,
  country: string,
  name: string,
  updatedAt: any,
}
