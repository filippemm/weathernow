import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'wtr-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.sass']
})
export class WeatherListComponent implements OnInit {

  constructor() { }

  public locations = ['Nuuk,GL', 'Urubici,BR', 'Nairobi,KE'];

  ngOnInit(): void {
  }

}
