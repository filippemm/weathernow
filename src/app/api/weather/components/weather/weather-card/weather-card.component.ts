import { Component, Input, OnInit, OnChanges, OnDestroy } from '@angular/core';

import { first } from 'rxjs/operators';
import { SimpleWeather } from '../../../interfaces/simple-weather';
import { OpenweathermapApiService } from '../../../resources/openweathermap-api.service';
import { WeatherStorageService } from '../../../services/weather-storage.service';

@Component({
  selector: 'wtr-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.sass']
})
export class WeatherCardComponent implements OnInit, OnChanges, OnDestroy {

  @Input() location!: string;

  public cardExpanded = false;
  public weather: SimpleWeather | undefined;
  public error = false;
  private cacheTiming: any;
  private cacheTimeAmount = 10 * 60 * 1000; // in milliseconds
  public cacheRefresh = true;

  constructor(
    protected $openweathermapApiService: OpenweathermapApiService,
    protected $weatherStorageService: WeatherStorageService,
  ) { }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    if (this.cacheTiming) { clearInterval(this.cacheTiming) }
  }

  ngOnChanges(): void {
    if (this.location) {
      this.setWeatherInfo(this.location)
    }
  }

  setCardExpanded(expanded: boolean) {
    this.cardExpanded = expanded
  }

  toggleCardExpanded() {
    this.cardExpanded = !this.cardExpanded
  }

  getTemperatureColor(temperature: number): string {
    if (temperature <= 5) {
      return 'color-blue'
    } else if (temperature <= 25) {
      return 'color-orange'
    }
    return 'color-red'
  }

  public async tryGetWeatherAgain() {
    this.error = false
    this.setWeatherInfo(this.location)
  }

  async setWeatherInfo(location: string): Promise<void> {
    const [name, country] = location.split(',')
    const storedWeather = this.$weatherStorageService.getLocationWeather(country, name)
    if (storedWeather) {
      this.weather = storedWeather;
      if (this.cacheRefresh) { this.getWeatherWithoutCache() }
      return
    }
    return this.setWeatherFromApi()
  }

  async setWeatherFromApi(): Promise<void> {
    try {
      const weatherToStore = await this.$openweathermapApiService.getWeatherByLocation(this.location).pipe(first()).toPromise()
      if (weatherToStore) {
        this.$weatherStorageService.setLocationWeather(weatherToStore)
        this.weather = weatherToStore
        if (this.cacheRefresh) { this.getWeatherWithoutCache() }
      }
    } catch (error) {
      this.error = true
    }
  }

  async getWeatherWithoutCache(): Promise<void> {
    if (this.cacheTiming) { clearInterval(this.cacheTiming) }
    console.log('cache');
    this.cacheTiming = setInterval(async () => {
      await this.setWeatherFromApi()
    }, this.cacheTimeAmount)
  }
}
