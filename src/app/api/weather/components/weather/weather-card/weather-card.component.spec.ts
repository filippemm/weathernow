import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, TestBed, tick, flush } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { Observable, of } from 'rxjs';
import { SimpleWeather } from '../../../interfaces/simple-weather';
import { OpenweathermapApiService } from '../../../resources/openweathermap-api.service';
import { WeatherStorageService } from '../../../services/weather-storage.service';

import { WeatherCardComponent } from './weather-card.component';

class MockWeatherStorageService {
  private weather: SimpleWeather | undefined;

  getLocationWeather(): SimpleWeather | undefined {
    return this.weather
  }

  insertLocationWeather(): void { }

}
class MockOpenweathermapApiService {
  getWeatherByLocation(location: string): Observable<SimpleWeather> {
    const response: SimpleWeather = {
      temp: 40,
      pressure: 500,
      humidity: 10,
      country: 'GL',
      name: 'Nuuk',
      updatedAt: 1623581088029
    }
    return of(response)
  }
}

describe('WeatherCardComponent', () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;
  let $openweathermapApiService: OpenweathermapApiService;
  let $weatherStorageService: WeatherStorageService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [WeatherCardComponent],
      providers: [
        { provide: OpenweathermapApiService, useClass: MockOpenweathermapApiService },
        { provide: WeatherStorageService, useClass: MockWeatherStorageService }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    $openweathermapApiService = TestBed.inject(OpenweathermapApiService)
    $weatherStorageService = TestBed.inject(WeatherStorageService)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set weather property when API Service is called', fakeAsync(() => {
    const location: string = 'Nuuk,GL';
    component.location = location;
    component.cacheRefresh = false

    component.setWeatherInfo('Nuuk,GL')
    fixture.detectChanges();
    tick(500);

    expect(component.weather?.temp).toBe(40);
    expect(component.weather?.humidity).toBe(10);
    expect(component.weather?.pressure).toBe(500);
    expect(component.weather?.name).toBe('Nuuk');
    expect(component.weather?.country).toBe('GL');
    expect(component.weather?.updatedAt).toBe(1623581088029);
    expect(component.error).toBeFalse();
  }))

  it('should show weather card complete', () => {
    const weather = {
      temp: 5,
      pressure: 1014,
      humidity: 87,
      country: 'GL',
      name: 'Nuuk',
      updatedAt: 1623581088029
    };
    component.weather = weather;
    component.cardExpanded = true;

    fixture.detectChanges();

    const description: HTMLElement = fixture.debugElement.query(By.css('.card-header')).nativeElement;
    const temperature: HTMLElement = fixture.debugElement.query(By.css('#temperature')).nativeElement;
    const humidity: HTMLElement = fixture.debugElement.query(By.css('#humidity')).nativeElement;
    const pressure: HTMLElement = fixture.debugElement.query(By.css('#pressure')).nativeElement;
    // const updatedAt: HTMLElement = fixture.debugElement.query(By.css('#update')).nativeElement;
    expect(description.innerHTML).toEqual(`${weather.name}, ${weather.country}`)
    expect(temperature.innerHTML).toEqual(`${weather.temp}º`)
    expect(humidity.innerHTML).toContain(`${weather.humidity}`)
    expect(humidity.innerHTML).toContain('%')
    expect(pressure.innerHTML).toContain(`${weather.pressure}`)
    expect(pressure.innerHTML).toContain('hPa')
    // expect(updatedAt.innerHTML).toEqual('Updated at 07:44:48 AM')
  })

  it('should hide card pressure and humidity information', () => {
    const weather = {
      temp: 5,
      pressure: 1014,
      humidity: 87,
      country: 'GL',
      name: 'Nuuk',
      updatedAt: 1623581088029
    };
    component.weather = weather;

    fixture.detectChanges();

    const humidity = fixture.debugElement.query(By.css('#humidity'));
    const pressure = fixture.debugElement.query(By.css('#pressure'));
    expect(humidity).toBeNull()
    expect(pressure).toBeNull()
  })

  it('should show temperature in blue color', () => {
    component.weather = {
      temp: 5,
      pressure: 1014,
      humidity: 87,
      country: 'GL',
      name: 'Nuuk',
      updatedAt: 1623581088029
    };

    fixture.detectChanges();

    const blueTemperature: HTMLElement = fixture.debugElement.query(By.css('.color-blue')).nativeElement;
    const orangeTemperature = fixture.debugElement.query(By.css('.color-orange'));
    const redTemperature = fixture.debugElement.query(By.css('.color-red'));
    expect(blueTemperature.innerHTML).toEqual('5º')
    expect(orangeTemperature).toBeNull()
    expect(redTemperature).toBeNull()
  })

  it('should show temperature in orange color', () => {
    component.weather = {
      temp: 25,
      pressure: 1014,
      humidity: 87,
      country: 'GL',
      name: 'Nuuk',
      updatedAt: 1623581088029
    };

    fixture.detectChanges();

    const blueTemperature = fixture.debugElement.query(By.css('.color-blue'));
    const orangeTemperature: HTMLElement = fixture.debugElement.query(By.css('.color-orange')).nativeElement;
    const redTemperature = fixture.debugElement.query(By.css('.color-red'));
    expect(blueTemperature).toBeNull()
    expect(orangeTemperature.innerHTML).toEqual('25º')
    expect(redTemperature).toBeNull()
  })

  it('should show temperature in red color', () => {
    component.weather = {
      temp: 26,
      pressure: 1014,
      humidity: 87,
      country: 'GL',
      name: 'Nuuk',
      updatedAt: 1623581088029
    };

    fixture.detectChanges();

    const blueTemperature = fixture.debugElement.query(By.css('.color-blue'));
    const orangeTemperature = fixture.debugElement.query(By.css('.color-orange'));
    const redTemperature: HTMLElement = fixture.debugElement.query(By.css('.color-red')).nativeElement;
    expect(blueTemperature).toBeNull()
    expect(orangeTemperature).toBeNull()
    expect(redTemperature.innerHTML).toEqual('26º')
  })

  it('should, in case of error, show the message in red and a button to try again', () => {
    component.error = true

    fixture.detectChanges();

    const redTemperature: HTMLElement = fixture.debugElement.query(By.css('.color-red')).nativeElement;
    const button: HTMLElement = fixture.debugElement.query(By.css('button')).nativeElement;
    expect(redTemperature.innerHTML).toEqual('Something went wrong')
    expect(button.innerHTML).toEqual('Try Again')
  })
});
