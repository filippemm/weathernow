import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { WeatherCardComponent } from './weather/weather-card/weather-card.component';
import { WeatherListComponent } from './weather/weather-list/weather-list.component';



@NgModule({
  declarations: [
    WeatherCardComponent,
    WeatherListComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    WeatherListComponent,
    WeatherCardComponent,
  ],
  providers: [
    HttpClient,
  ]
})
export class ComponentsModule { }
