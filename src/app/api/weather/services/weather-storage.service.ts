import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';

import { SimpleWeather } from '../interfaces/simple-weather';

const WEATHERKEY = 'locationsweather'

@Injectable({
  providedIn: 'root'
})
export class WeatherStorageService {

  getLocationWeather(country: string, name: string): SimpleWeather | null {
    const locationStorageKey = this.getLocationKey(country, name);
    const locationWeather = window.localStorage.getItem(locationStorageKey);
    if (!locationWeather) {
      return null
    }
    return JSON.parse(locationWeather);
  }

  setLocationWeather(weather: SimpleWeather): void {
    const locationStorageKey = this.getLocationKey(weather.country, weather.name);
    window.localStorage.setItem(locationStorageKey, JSON.stringify(weather));
  }

  getLocationKey(country: string, name: string) {
    return `${WEATHERKEY}${name}${country}`
  }
}
