import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SimpleWeather } from '../interfaces/simple-weather';

@Injectable({
  providedIn: 'root'
})
export class OpenweathermapApiService {

  protected baseUrl = `https://api.openweathermap.org/data/2.5/weather?appid=${environment.openWeatherMapKey}&units=metric&q=`

  constructor(
    protected $http: HttpClient,
  ) { }

  getWeatherByLocation(location: string): Observable<SimpleWeather> {
    return this.$http.request<SimpleWeather>('GET', `${this.baseUrl}${location}`).pipe(
      map((locationWeather: any) => {
        return {
          temp: Math.round(locationWeather.main.temp),
          pressure: locationWeather.main.pressure,
          humidity: locationWeather.main.humidity,
          country: locationWeather.sys.country,
          name: locationWeather.name,
          updatedAt: Date.now()
        }
      })
    )
  } 
}
