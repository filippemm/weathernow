import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WeatherNowComponent } from './weather-now/weather-now.component';

const routes: Routes = [
  {
    path: '',
    component: WeatherNowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule { }
