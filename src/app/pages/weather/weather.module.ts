import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherNowComponent } from './weather-now/weather-now.component';
import { ComponentsModule } from 'src/app/api/weather/components/components.module';


@NgModule({
  declarations: [
    WeatherNowComponent
  ],
  imports: [
    CommonModule,
    WeatherRoutingModule,
    ComponentsModule,
  ],
})
export class WeatherModule { }
