import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'wtr-weather-now',
  templateUrl: './weather-now.component.html',
  styleUrls: ['./weather-now.component.sass']
})
export class WeatherNowComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
